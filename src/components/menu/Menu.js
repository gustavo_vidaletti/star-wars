import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import SWApi from '../../api/SWApi';
import * as moment from 'moment';

import './Menu.css';
import '../../utils/number-prototype';
import '../../utils/array-prototype';
//*$ST&ar<br/>wa^[RS]

export default class Menu extends Component {

  constructor(props) {
    super(props);
    this.SWApi = new SWApi();
    this.state = {
      loader: true,
      filmes: []
    }
  }

  componentDidMount() {
    this.SWApi.getFilms().then(resp => {
      console.log(resp.results)
      setTimeout(() => {
        this.setState({
          loader: false,
          filmes: resp.results.ordenarEpisodiosPorId()
        })
      }, 1500)
    }
    );
  }

  render() {
    const { loader, filmes } = this.state;
    if (loader) return (<div className="container centralizado background"><div className="long-time-ago azul">Loading....</div></div>)
    return (
      <div className="container centralizado background">
        <div className="menu" style={{animation: 'desaparecer 0.5s ease forwards reverse 0s'}}>
          <div className="logo amarelo" style={{padding: '20px', margin: '20px'}}>
            *$ST&ar<br />wa^[RS]
          </div>
          {filmes.map(filme => {
            return (
              <div key={filme.episode_id} className="bloco">
                <div className="linha titulo">
                  Episódio {filme.episode_id.toRoman()} - {filme.title}
                </div>
                <div className="linha">
                  <div className="metade">
                    Estréia: {moment(filme.release_date).format("DD/MM/YYYY")}
                  </div>
                  <div className="metade">
                    Diretor: {filme.director}
                  </div>
                </div>
                <div className="linha">
                  <Link className="btnRedirect" to={`/entrada/${filme.episode_id}`} >
                    SINOPSE
                  </Link>
                </div>

              </div>
            )
          })}

        </div>
      </div>
    )
  }
}