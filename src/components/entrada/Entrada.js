import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect } from 'react-router-dom';
import SWApi from '../../api/SWApi';
import Texto from '../texto/Texto';
//*$ST&ar<br/>wa^[RS]

export default class Entrada extends Component {
  constructor(props) {
    super(props);
    this.SWApi = new SWApi();
    this.state = {
      episodio: { title: '', episode_id: '', opening_crawl: '' },
      loader: true,
      animacaoLogo: false,
      background: false,
      texto: false,
      redirecionar: false
    }
  }

  componentDidMount() {
    this.entrada()
  }

  componentWillReceiveProps = (props) => {
    this.props = props
    this.setState({
      episodio: { title: '', episode_id: '', opening_crawl: '' },
      loader: true,
      animacaoLogo: false,
      background: false,
      texto: false,
      redirecionar: false
    })
    this.entrada();
  }

  entrada = () => {
    const { id } = this.props.match.params
    this.SWApi.getConteudoCompleto(id).then(resposta => {
      console.log(resposta[0])
      this.setState({
        episodio: resposta[0]
      });
    })

    setTimeout(() => {
      this.setState({
        loader: false,
      });

      setTimeout(() => {
        this.setState({
          animacaoLogo: true,
          background: true
        });
        setTimeout(() => {
          this.setState({
            texto: true
          });
        }, 2800)
        setTimeout(() => {
          this.setState({
            animacaoLogo: false,
          });
          setTimeout(() => {
            this.setState({
              redirecionar: true
            });
          }, 34000);
        }, 6200);
      }, 1200);
    }, 3800);
  }

  render() {
    const { loader, background, animacaoLogo, texto, episodio, redirecionar } = this.state;
    if (loader) return (
      <div className="container">
        <h1 className={`long-time-ago azul loader-inicial`}>A long time ago in a galaxy far,<br /> far away...
      </h1>
      </div>
    )
    if (redirecionar) return (
      <>
        <Router>
          <Redirect to="/" />
        </Router>
      </>
    )
    return (
      <div className={`container ${background ? 'background' : ''}`}>
        {
          (animacaoLogo) ?
            <div className="logo amarelo entrada">
              *$ST&ar<br />wa^[RS]
          </div>
            : ''
        }
        {
          (texto) ? (
            <>
              <Texto texto={episodio.opening_crawl} tituloEpisodio={episodio.title} idEpisodio={episodio.episode_id} />
            </>
          ) : ''
        }

      </div>
    );
  }
}