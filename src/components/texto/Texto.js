import React, { Component } from 'react';
import './Texto.css';
import PropTypes from 'prop-types';
import '../../utils/number-prototype'
//*$ST&ar<br/>wa^[RS]

export default class Texto extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.idEpisodio,
      titulo: this.props.tituloEpisodio,
      texto: this.props.texto
    }
  }

  render() {
    const { id, texto, titulo } = this.state;
    return (
      <>
        <div className="outside-entrada">
          <div className="numeracao amarelo">
            Episode {Number.parseInt(id).toRoman()}
          </div>
          <div className="titulo amarelo">
            {titulo}
          </div>
          <div className="texto amarelo">
            {texto}
          </div>
        </div>
      </>
    );
  }
}

Texto.propTypes = {
  idEpisodio: PropTypes.number.isRequired,
  tituloEpisodio: PropTypes.string.isRequired,
  texto: PropTypes.string.isRequired
}