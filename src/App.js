import React, { Component } from 'react';
import { BrowserRouter as Router, Route, /*Redirect, Switch*/ } from 'react-router-dom';
import SWApi from './api/SWApi';
import Menu from './components/menu/Menu';
import Entrada from './components/entrada/Entrada';

import './utils/array-prototype';
import './utils/number-prototype';
import './img/background.png';
import './fonts/Starjhol.ttf';
import './App.css';
//*$ST&ar<br/>wa^[RS]

class App extends Component {
  constructor(props) {
    super(props);
    this.SWApi = new SWApi();
    this.state = {
      loader: true,
    }
  }

  render() {
    return (
      <>
        {/* <div className="container"><h1 className={`logo azul`}>star <br/>wars</h1></div> */}
        <Router>
          <Route path="/" render={(props) => <Menu {...props} />} />
          <Route path="/entrada/:id" render={(props) => <Entrada {...props} />} />
        </Router>
      </>
    );
  }
}

export default App;
