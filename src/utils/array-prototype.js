/* eslint-disable no-extend-native */
Array.prototype.ordenarEpisodiosPorId = function(){
  return this.sort( (a, b) => {
    return a.episode_id - b.episode_id;
  })
}
