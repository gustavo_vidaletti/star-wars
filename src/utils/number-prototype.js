/* eslint-disable no-extend-native */
Number.prototype.toRoman = function(){
  const romanNum = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
  const dNum = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  let value = this;
    if (value <= 0 || value >= 4000)
      return value;
    var romanNumeral = "";
    for (var i = 0; i < romanNum.length; i++) {
      while (value >= dNum[i]) {
        value -= dNum[i];
        romanNumeral += romanNum[i];
      }
    }
    return romanNumeral;
}