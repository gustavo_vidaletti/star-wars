import axios from 'axios';
import '../utils/array-prototype';

const urlBase = 'https://swapi.co/api/';

export default class SWApi {

  async getPadrao(url = ''){
    const conteudoLocal = JSON.parse(localStorage.getItem(url));
    if(conteudoLocal){
      return conteudoLocal;
    }
    const resposta = await axios({
      method: 'get',
      url: `${url}`
    }).catch(() => {
      return null
    });
    if(resposta) localStorage.setItem(url, JSON.stringify(resposta.data))
    return (resposta) ? resposta.data : null;
  }

  async requestPadrao(parametros = '') {
    return await this.getPadrao(`${urlBase}${parametros}`);
  }

  async getFilms(id = '') {
    return await this.requestPadrao(`films/${id}`);
  }

  async getPeople(id = '') {
    return await this.requestPadrao(`people/${id}`);
  }

  async getPlanets(id = '') {
    return await this.requestPadrao(`planets/${id}`);
  }

  async getSpecies(id = '') {
    return await this.requestPadrao(`species/${id}`);
  }

  async getStarships(id = '') {
    return await this.requestPadrao(`starships/${id}`);
  }

  async getVehicles(id = '') {
    return await this.requestPadrao(`vehicles/${id}`);
  }

  async getConteudoCompleto(id = ''){
    const resultado = await this.getFilms();
    const filmes = (id) ? resultado.results.filter( f => f.episode_id.toString() === id.toString()) : resultado.results
    
    filmes.ordenarEpisodiosPorId();
    filmes.map( async filme => {
      filme.characters = await Promise.all(filme.characters.map( async personagem => await this.getPadrao(personagem)));
      filme.planets = await Promise.all(filme.planets.map( async planeta => await this.getPadrao(planeta)));
      filme.species = await Promise.all(filme.species.map( async especie => await this.getPadrao(especie)));
      filme.starships = await Promise.all(filme.starships.map( async nave => await this.getPadrao(nave)));
      filme.vehicles = await Promise.all(filme.vehicles.map( async veiculo => await this.getPadrao(veiculo)));
      return filme;
    });

    
    return filmes;
  }

}